var World = require('../lib/world');

var country2 = World.findCountryByCode('BE'),
    country3 = World.findCountryByCode('BEL'),
    countryName = World.findCountryByName('Belgium');

console.log('Country alpha 2 code : %s', country2.getAlpha2Code());
console.log('Country alpha 3 code : %s', country3.getAlpha3Code());
console.log('Country codes : %s', countryName.getName());
console.log('Country name : %s', JSON.stringify(country2.getCodes()));
console.log('Country name in English : %s', country3.getLocalizedNameIn('en'));
console.log('Country name in French : %s', countryName.getLocalizedNameIn('fr'));
console.log('Country name in Dutch : %s', country2.getLocalizedNameIn('nl'));
console.log('Country name in German : %s', country3.getLocalizedNameIn('de'));
console.log('Country names : %s', JSON.stringify(countryName.getLocalizedNames()));