var fs = require('fs'),
    path = require('path');

var FILES_PATH = path.join(__dirname, '..', 'files');

exports.load = function (filename) {
    var filePath = path.join(FILES_PATH, filename + '.json');
    return JSON.parse(fs.readFileSync(filePath, { encoding: 'utf8' }));
};