# node-world

[![NPM](https://nodei.co/npm/node-world.png?downloads=true&stars=true)](https://nodei.co/npm/node-world/)

Utility to work with countries data.

## Features

* European countries (for now)
* 4 languages for countries localized name (french, english, dutch and german)

## Future

* Add web interface to manage countries data
* Add more countries
* Add more languages
* Add more data about each countries

Feel free to contribute.

## Installation

```
$ npm install node-world
```

## Quick Start

### Load in the module

```js
var World = require('node-world');
```

### Retrieve country object

You can find a country by code, ISO 3166-1-alpha-2 or ISO 3166-1-alpha-3, and by name.

```js
var country = World.findCountryByCode('SP'); // ISO 3166-1-alpha-2
var country = World.findCountryByCode('ESP'); // ISO 3166-1-alpha-3

var country = World.findCountryByName('Spain'); // Country name
```

### Country methods

* **getAlpha2Code()** : Retrieve ISO 3166-1-alpha-2.

```
SP
```

* **getAlpha3Code()** : Retrieve ISO 3166-1-alpha-3.

```
ESP
```

* **getCodes()** : Retrieve a JSON object which contains the 2 alpha codes.

```
{
	"alpha_2_code": "ES",
	"alpha_3_code": "ESP"
}
```

* **getName()** : Retrieve international country name.

```
Spain
```

* **getLocalizedNames()** : Retrieve localized names.

```
{
	"fr": "Espagne",
	"en": "Spain",
	"de": "Spanien",
	"nl": "Spanje"
}
```

* **getLocalizedNameIn( language )** : Retrieve a localized name by language. If the localized name does not exist this method will return the international country name.

```
Spain
```

## Tests

```
$ npm run-script be-data
```